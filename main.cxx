#include "main.hxx"

int main(int argc, char **argv)
{
  QCoreApplication app(argc, argv);
  QTextStream out(stdout);

  if (4 == argc)
    {
      std::ifstream inFile(argv[1]);
      if (inFile.fail()) {
            qDebug() << "Unable to open file " << argv[1] << " for reading.";
            app.exit(EXIT_FAILURE);
        }
      std::ofstream outFile(argv[2]);
      if (inFile.fail()) {
            qDebug() << "Unable to open file " << argv[2] << " for writing.";
            app.exit(EXIT_FAILURE);
        }
      int nspaces = strtol(argv[3], nullptr, 10);
      if (inFile.is_open() && outFile.is_open())
        {
          char c;
          while (inFile.get(c))
            {
              if ('\t' == c)
                {
                  for (auto i = 0; i != nspaces; ++i)
                    {
                      outFile << ' ';
                    }
                }
              else
                {
                  outFile << c;
                }
            }
          inFile.close();
          outFile.close();
        }
      else
        {
          qDebug() << "Unable to open files";
          app.exit(EXIT_FAILURE);
        }
    }
  else
    {
      qDebug() << "Use: TabsToSpaces [infile] [outfile] [nspaces]";
      app.exit(EXIT_FAILURE);
    }

  return EXIT_SUCCESS;
}

