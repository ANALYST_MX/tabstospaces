QT += core
QT -= gui

TARGET = TabsToSpaces
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cxx

HEADERS += \
    main.hxx

CONFIG += c++11
